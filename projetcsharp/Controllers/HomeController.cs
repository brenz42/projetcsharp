﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MySqlConnector;
using projetcsharp.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace projetcsharp.Controllers
{
	public class HomeController : Controller
	{
		private readonly ILogger<HomeController> _logger;

		public HomeController(ILogger<HomeController> logger)
		{
			_logger = logger;
		}

		public IActionResult Index()
		{
			MySqlConnectionStringBuilder connectionBuilder = new MySqlConnectionStringBuilder
			{
				Server = "localhost",
				Port = 3306,
				UserID = "valentin",
				Password = "root",
				Database = "test"
			};
			string hello = null;

			using (MySqlConnection connection = new MySqlConnection(connectionBuilder.ToString()))
			{
				connection.Open();

				using (MySqlCommand command = new MySqlCommand("SELECT * FROM hello_world LIMIT 1;", connection))
				{
					using (MySqlDataReader reader = command.ExecuteReader())
					{
						if (reader.Read())
							hello = reader.GetString("mot");
					}
				}
			}
			ViewBag.Hello = hello;
			return View();
		}

		public IActionResult Privacy()
		{
			return View();
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}
	}
}
